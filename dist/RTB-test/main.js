(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-email-form></app-email-form>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _email_form_email_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./email-form/email-form.component */ "./src/app/email-form/email-form.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _email_form_email_form_component__WEBPACK_IMPORTED_MODULE_4__["EmailFormComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/email-form/email-form.component.html":
/*!******************************************************!*\
  !*** ./src/app/email-form/email-form.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"b-add-email\">\n    <div class=\"b-add-email__title\">\n        Share <span>Board name</span> with other\n    </div>\n    <div class=\"b-add-email__input\">\n        <div class=\"email-block\" *ngFor=\"let email of emails\" [ngClass]=\"{valid:email.isValid}\">\n            {{email.value}}\n            <button class=\"delete-btn\" (click)='deleteEmail(email)'></button>\n        </div>\n\n        <form (submit)='addEmail(emailInput)'>\n            <input id=\"email\" name=\"email\" placeholder=\"add more email...\" type=\"email\" [(ngModel)]='emailInput'\n                   (ngModelChange)='changeInput()' (blur)='addEmail(emailInput)'\n                   (paste)=' pasteCheck($event)'>\n        </form>\n    </div>\n</div>\n<div class=\"b-btns\">\n    <button class=\"btn\" (click)=\"randomEmail()\">Add random email</button>\n    <button class=\"btn\" (click)=\"checkCount()\">Get emails count</button>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/email-form/email-form.component.scss":
/*!******************************************************!*\
  !*** ./src/app/email-form/email-form.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".b-add-email {\n  width: 500px;\n  height: 220px;\n  padding: 29px 40px;\n  background-color: #f5f5f5; }\n\n.b-add-email__title {\n  font: 400 20px \"Open Sans\", sans-serif; }\n\n.b-add-email__title span {\n    font-weight: 700; }\n\n.b-add-email__input {\n  background-color: white;\n  height: 61%;\n  width: 100%;\n  margin-top: 35px;\n  overflow-y: auto;\n  padding: 7px 8px; }\n\n.b-add-email__input form {\n    display: inline-block;\n    width: 50%; }\n\n.b-add-email__input input {\n    width: 100%;\n    font: 400 14px \"Open Sans\", sans-serif;\n    border: none;\n    padding: 2px 0; }\n\n.b-add-email__input input:-webkit-autofill {\n      -webkit-box-shadow: inset 0 0 0 50px #ffffff !important;\n      -webkit-text-fill-color: #333333 !important;\n      color: #333333 !important; }\n\n.b-add-email__input .email-block {\n    display: inline-block;\n    font: 400 14px \"Open Sans\", sans-serif;\n    border-radius: 20px;\n    padding: 2px 28px 2px 10px;\n    position: relative;\n    margin-right: 10px;\n    margin-bottom: 10px;\n    background-color: rgba(255, 73, 46, 0.35); }\n\n.b-add-email__input .email-block .delete-btn {\n      border: none;\n      width: 8px;\n      height: 8px;\n      position: absolute;\n      top: 50%;\n      right: 10px;\n      -webkit-transform: translateY(-50%);\n              transform: translateY(-50%);\n      background: url(\"data:image/svg+xml,%3C%3Fxml version%3D%221.0%22 encoding%3D%22iso-8859-1%22%3F%3E%3C!-- Generator%3A Adobe Illustrator 19.0.0%2C SVG Export Plug-In . SVG Version%3A 6.00 Build 0)  --%3E%3Csvg xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22 xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22 version%3D%221.1%22 id%3D%22Capa_1%22 x%3D%220px%22 y%3D%220px%22 viewBox%3D%220 0 47.971 47.971%22 style%3D%22enable-background%3Anew 0 0 47.971 47.971%3B%22 xml%3Aspace%3D%22preserve%22 width%3D%22512px%22 height%3D%22512px%22%3E%3Cg%3E%09%3Cpath d%3D%22M28.228%2C23.986L47.092%2C5.122c1.172-1.171%2C1.172-3.071%2C0-4.242c-1.172-1.172-3.07-1.172-4.242%2C0L23.986%2C19.744L5.121%2C0.88   c-1.172-1.172-3.07-1.172-4.242%2C0c-1.172%2C1.171-1.172%2C3.071%2C0%2C4.242l18.865%2C18.864L0.879%2C42.85c-1.172%2C1.171-1.172%2C3.071%2C0%2C4.242   C1.465%2C47.677%2C2.233%2C47.97%2C3%2C47.97s1.535-0.293%2C2.121-0.879l18.865-18.864L42.85%2C47.091c0.586%2C0.586%2C1.354%2C0.879%2C2.121%2C0.879   s1.535-0.293%2C2.121-0.879c1.172-1.171%2C1.172-3.071%2C0-4.242L28.228%2C23.986z%22 fill%3D%22%23333333%22%2F%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3C%2Fsvg%3E\") center/cover no-repeat;\n      cursor: pointer; }\n\n.b-add-email__input .email-block.valid {\n      background-color: #d4e4ff; }\n\n.b-btns {\n  padding: 30px; }\n\n.b-btns .btn {\n    border: none;\n    border-radius: 3px;\n    margin: 0 10px;\n    display: inline-block;\n    padding: 8px 16px;\n    background-color: #6699ff;\n    cursor: pointer;\n    font: 400 14px \"Open Sans\", sans-serif;\n    color: white; }\n"

/***/ }),

/***/ "./src/app/email-form/email-form.component.ts":
/*!****************************************************!*\
  !*** ./src/app/email-form/email-form.component.ts ***!
  \****************************************************/
/*! exports provided: EmailFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailFormComponent", function() { return EmailFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EmailFormComponent = /** @class */ (function () {
    function EmailFormComponent() {
        var _this = this;
        this.emails = [];
        this.addEmail = function (email) {
            var validateEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var emailElem = {
                value: email,
                isValid: validateEmail.test(email)
            };
            var elemIndex = _this.emails.findIndex(function (i) { return i.value === emailElem.value; });
            if (emailElem.value.length > 3 && emailElem.value.trim() !== '' && emailElem.value !== '' && elemIndex === -1) {
                _this.emails.push(emailElem);
                _this.emailInput = '';
            }
            else {
                _this.emailInput = '';
            }
        };
        this.changeInput = function () {
            var lastSymbol = _this.emailInput.substr(-1);
            var penultTwoSymbol = _this.emailInput.substr(-2);
            if (lastSymbol === '') {
                _this.emailInput = ' ';
            }
            else if (lastSymbol === ',') {
                if (penultTwoSymbol === ',,') {
                    _this.emailInput = ' ';
                }
                else {
                    _this.addEmail(_this.emailInput.slice(0, -1));
                }
            }
        };
        this.checkCount = function () {
            var countEmail = _this.emails.length;
            alert('Count of emails: ' + countEmail);
        };
        this.deleteEmail = function (emailElem) {
            var elemIndex = _this.emails.findIndex(function (i) { return i.value === emailElem.value; });
            _this.emails.splice(elemIndex, 1);
        };
        this.randomEmail = function () {
            var login = Math.random().toString(36).slice(2, 2 + Math.max(1, Math.min(10, 10)));
            var domenName = Math.random().toString(36).slice(2, 2 + Math.max(1, Math.min(6, 10)));
            var email = login + '@' + domenName + '.ru';
            _this.addEmail(email);
        };
        this.pasteCheck = function (ev) {
            var email = ev.clipboardData.getData("text/plain");
            _this.addEmail(email);
            return false;
        };
    }
    EmailFormComponent.prototype.ngOnInit = function () {
        this.emailInput = '';
    };
    EmailFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-email-form',
            template: __webpack_require__(/*! ./email-form.component.html */ "./src/app/email-form/email-form.component.html"),
            styles: [__webpack_require__(/*! ./email-form.component.scss */ "./src/app/email-form/email-form.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EmailFormComponent);
    return EmailFormComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\BItBucket\AngularTest\RTB-test\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map