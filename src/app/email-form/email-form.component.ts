import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-email-form',
    templateUrl: './email-form.component.html',
    styleUrls: ['./email-form.component.scss']
})
export class EmailFormComponent implements OnInit {

    emailInput: string;
    emails = [];

    constructor() {
    }

    ngOnInit() {
        this.emailInput = '';
    }

    addEmail = (email) => {
        const validateEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        const emailElem = {
            value: email,
            isValid: validateEmail.test(email)
        };
        const elemIndex = this.emails.findIndex(i => i.value === emailElem.value);
        if (emailElem.value.length > 3 && emailElem.value.trim() !== '' && emailElem.value !== '' && elemIndex === -1) {
            this.emails.push(emailElem);
            this.emailInput = '';
        } else {
            this.emailInput = '';
        }
    }

    changeInput = () => {
        const lastSymbol = this.emailInput.substr(-1);
        const penultTwoSymbol = this.emailInput.substr(-2);
        if (lastSymbol === '') {
            this.emailInput = ' ';
        } else if (lastSymbol === ',') {
            if (penultTwoSymbol === ',,') {
                this.emailInput = ' ';
            } else {
                this.addEmail(this.emailInput.slice(0, -1));
            }
        }
    }

    checkCount = () => {
        const countEmail = this.emails.length;
        alert('Count of emails: ' + countEmail);
    }

    deleteEmail = (emailElem) => {
        const elemIndex = this.emails.findIndex(i => i.value === emailElem.value);
        this.emails.splice(elemIndex, 1);
    }

    randomEmail = () => {
        const login = Math.random().toString(36).slice(2, 2 + Math.max(1, Math.min(10, 10)));
        const domenName = Math.random().toString(36).slice(2, 2 + Math.max(1, Math.min(6, 10)));
        const email = login + '@' + domenName + '.ru';
        this.addEmail(email);
    }

    pasteCheck = (ev) => {
        const email = ev.clipboardData.getData("text/plain");
        this.addEmail(email);
        return false;
    }
}